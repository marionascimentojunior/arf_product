<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // trata erro 404 na aplicação de modo geral
        if ($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            // retorna erro apenas da requisição API
            if ($request->expectsJSON()) {
                return response()->json(['error' => 'Recurso URI nao encontrado.'], 404);
            }            
        }

        // trata erro 405 de verbo Http não suportado pela rota requisitada
        if ($exception instanceof \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException) {
            // retorna erro apenas da requisição API
            if ($request->expectsJSON()) {
                return response()->json(['error' => 'Verbo Http nao supotado pela rota.'], $exception->getStatusCode());
            }
        }

        if ($exception instanceof Tymon\JWTAuth\Exceptions\TokenExpiredException) 
            return response()->json(['token_expired'], $exception->getStatusCode());

        if ($exception instanceof Tymon\JWTAuth\Exceptions\TokenInvalidException) 
            return response()->json(['token_invalid'], $exception->getStatusCode());
        


        return parent::render($request, $exception);
    }
}
