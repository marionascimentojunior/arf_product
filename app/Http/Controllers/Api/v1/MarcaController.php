<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Marca;
use App\Http\Requests\StoreUpdateMarcaFormRequest;

class MarcaController extends Controller {
    // torna o atributo acessível a todos os métodos do Controller afim de não repetir código
    private $marca;
    private $pages = 10;

    public function __construct(Marca $marca) {
        $this->marca = $marca;
    }

    public function index(Request $request) {  
        $marcas = $this->marca->getMarcas($request->nome);

        return response()->json($marcas);
    }

    public function show($id) {
        // se não encontrar a marca, retorna o status de 404
        if (!$marcas = $this->marca->find($id))
            return response()->json(['erros' => 'Not found'], 404);
           
        return response()->json($marcas, 200);
    }

    public function store(StoreUpdateMarcaFormRequest $request) {
        $marcas = $this->marca->create($request->all());

        return response()->json($marcas, 201);
    }

    public function update(StoreUpdateMarcaFormRequest $request, $id) {
        // se não encontrar a marca, retorna o status de 404
        if(!$marcas = $this->marca->find($id))
            return response()->json(['erros' => 'Not found'], 404);
        
        $marcas->update($request->all());

        return response()->json($marcas);
    }

    public function destroy($id) {
        // verifica se encontrou o id a ser deletado
        if(!$marcas = $this->marca->find($id))
            return response()->json(['error' => 'Not found'], 404);
        
        $marcas->delete();

        return response()->json(['deletado com sucesso' => true], 204);
    }

    public function produtos($id) {
        //if (!$marcas = $this->marca->find($id)) 
        // trazendo os dados com os relacionamentos
        if(!$marcas = $this->marca->find($id))
            return response()->json(['error' => 'Not found'], 404);

        $produtos = $marcas->produtos()->paginate($this->pages);
        return response()->json([
            'marca'     => $marcas,
            'produtos'  => $produtos,
        ]);
    }
}
