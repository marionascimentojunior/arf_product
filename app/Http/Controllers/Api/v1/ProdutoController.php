<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Produto;
use App\Http\Requests\StoreUpdateProdutoFormRequest;
use Illuminate\Support\Facades\Storage;


class ProdutoController extends Controller
{
    private $produto;
    private $totPage = 5;
    private $path = 'produtos';

    public function __construct(Produto $produto) {
        $this->produto = $produto;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $produtos = $this->produto->getProdutos($request->all(), $this->totPage);

        return response()->json($produtos);
    }    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateProdutoFormRequest $request)
    {
        $data = $request->all();
        
        // verifica se existe imagem para upload e se o arquivo não está corrompido
        if($request->hasFile('foto') && $request->file('foto')->isValid()) {
            
            $name = kebab_case($request->nome);
            $ext = $request->foto->extension();

            $nomeArquivo = "{$name}.{$ext}";
            $data['foto'] = $nomeArquivo;

            // fazer upload
            $upload = $request->foto->storeAs('produtos', $nomeArquivo);
            if(!$upload)
                return response()->json(['error' => 'Fail Upload'], 500);
        }

        $produto = $this->produto->create($data);
        return response()->json($produto, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if(!$produto = $this->produto->with(['marca'])->find($id))
            return response()->json(['error' => 'Not found'], 404);
        
        return response()->json($produto);
    }   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateProdutoFormRequest $request, $id) {
        if(!$produto = $this->produto->find($id))
            return response()->json(['error' => 'Not found'], 404);

        $data = $request->all();


        // verifica se existe imagem para upload e se o arquivo não está corrompido
        if ($request->hasFile('foto') && $request->file('foto')->isValid()) {

            // se a imagem já existe, deleta
            if($produto->foto) {
                if(Storage::exists("{$this->path}/{$produto->foto}"))
                   Storage::delete("{$this->path}/{$produto->foto}");
            } 

            $name = kebab_case($request->nome);
            $ext = $request->foto->extension();

            $nomeArquivo = "{$name}.{$ext}";
            $data['foto'] = $nomeArquivo;

            // fazer upload
            $upload = $request->foto->storeAs($this->path, $nomeArquivo);
            if (!$upload) {
                return response()->json(['error' => 'Fail Upload'], 500);
            }

        }

        $produto->update($data);
        return response()->json($produto);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (!$produto = $this->produto->find($id)) 
            return response()->json(['error' => 'Not found'], 404);

        if($produto->foto) {
            if(Storage::exists("{$this->path}/{$produto->foto}"))
               Storage::delete("{$this->path}/{$produto->foto}");
        }

        $produto->delete();
        return response()->json(['deletado com sucesso', true], 204);
    }
}
