<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateProdutoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);
        return [
            'marca_id'  => 'required|exists:marcas,id',
            'nome'      => "required|min:2|max:30|unique:produtos,nome,{$id},id",
            'detalhe'   => 'max:1024',
            'foto'      => 'image',
        ];
    }
}
