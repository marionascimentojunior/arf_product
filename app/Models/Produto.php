<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $fillable = ['marca_id', 'nome', 'detalhe', 'foto'];

    public function getProdutos($data, $pages) {
        if(!isset($data['filter']) && !isset($data['nome']) && !isset($data['detalhe']))
            return $this->paginate($pages);

        return $this->where(function($query) use ($data) {
                    if(isset($data['filter'])) {
                        $filter = $data['filter'];
                        $query->where('nome', '=', $filter);
                        $query->orWhere('detalhe', 'LIKE', "%{$filter}%");
                    }

                    if(isset($data['nome']))
                        $query->where('nome', '=', $data['nome']);

                    if(isset($data['detalhe'])) {
                        $detalhe = $data['detalhe'];
                        $query->where('detalhe', 'LIKE', "%{$detalhe}%");
                    }            
                })
                ->paginate($pages);
    }

    public function marca() {
        return $this->belongsTo(Marca::class);
    }
}
