<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\Models\Produto;

$factory->define(Produto::class, function (Faker $faker) {
    return [
        'marca_id'  => 1,
        'nome'      => $faker->unique(true)->word,
        'detalhe'   => $faker->sentence(),
    ];
});
