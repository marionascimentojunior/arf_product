<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// rota para acessar as marcas do produto
// Route::get('marcas', 'Api\MarcaController@index');
// Route::post('marcas', 'Api\MarcaController@store');
// Route::put('marcas/{id}', 'Api\MarcaController@update');
// Route::delete('marcas/{id}', 'Api\MarcaController@delete');

// configurando rotas simplificadas
/*
    A rota resource entende os métodos:
    index - para listar
    store - para cadastrar
    update - para editar
    destroy - para deletar
*/

Route::post('auth', 'Auth\AuthApiController@login');
Route::get('user', 'Auth\AuthApiController@getAuthenticatedUser');
Route::get('auth-refresh', 'Auth\AuthApiController@refreshToken');

// Criando grupo de rotas para versionamentos futuros
Route::group(
    [
        'prefix'        => 'v1', 
        'namespace'     => 'Api\v1',
        'middleware'    => 'jwt.auth'
    ], function () {
        Route::resource('marcas', 'MarcaController');
        Route::resource('produtos', 'ProdutoController');
        Route::get('marcas/{id}/produtos', 'MarcaController@produtos');    
});
